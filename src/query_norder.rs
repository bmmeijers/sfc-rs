use relate;
use std::collections::VecDeque;
use std::env;


fn nary(dim: u32) -> usize
{
    // how many childs does a nd-box have given the number of dimensions
    2usize.pow(dim)
}

fn path_to_morton(path: &Vec<u64>, dim: usize, order: usize) -> [u64; 2]
{
    let mut diff = order - 1;
    let mut start = 0;
    for zorder in path {
        let width = diff * dim;
        start += zorder << width;
        if diff > 1 {
            diff -= 1;
        }
    }
    let level = path.len();
    let width = 2u64.pow( (dim * (order-level)) as u32);
    [start, start + width]
}

fn _nquery(query: relate::NdBox, maxdepth: usize, maxbits: usize) -> (Vec<[u64; 2]>) {
    let dim = query.dims();
    let order = (maxbits / dim) as u32;
    // let maxdepth = 12;
    let l_root = vec![0; dim];
    let h_root = vec![2u64.pow(order); dim];
    let root = relate::NdBox::new(l_root, h_root);
//    println!("{}", root);
    let mut deq = VecDeque::new();
    deq.push_back( (root, 1, vec![0u64;0]) );
    let mut ranges = vec![];
    while deq.len() > 0 {
        let value = match deq.pop_front() {
            Some(value) => value,
            None => panic!("Popping from empty VecDeque")
        };
        let cur_node = value.0;
        let cur_level = value.1;
        let cur_path = value.2;
        let ndcmp = query.relate(&cur_node);
        match ndcmp {
            // -- disjoint, done: no need to process further
            relate::RelateResult::Disjoint => {},
            // -- equal or fully contains, done: add to result
            relate::RelateResult::Equals | relate::RelateResult::FullyContains => {
                ranges.push(path_to_morton(&cur_path, dim, order as usize));
            },
            // -- partial overlap
            relate::RelateResult::Partial => {
                if cur_level < maxdepth {
                    for ncode in 0.. nary(dim as u32)
                    {
                        let mut new_path = cur_path.clone();
                        new_path.push(ncode as u64);
                        deq.push_back((cur_node.nth_child_norder(ncode), cur_level + 1, new_path));
                    }
                } else {
                    ranges.push(path_to_morton(&cur_path, dim, order as usize));
                };
            }
        };
    }

    ranges
}


pub fn nquery(low: Vec<u64>, high: Vec<u64>, maxdepth: usize) -> Vec<[u64; 2]> {
    let bx = relate::NdBox::new(low, high);
//                                vec![1066052, 1642769, 1899], 
//                                vec![1083529, 1677722, 2057]);
    //    let bx = relate::NdBox::new(vec![0; dims], vec![512; dims]);
    let ranges = _nquery(bx, maxdepth, 63);
    ranges
}


pub fn main() {
    let args: Vec<_> = env::args().collect();
    if args.len() > 1 {
        let maxdepth: usize = args[1].parse().unwrap();
        assert!(maxdepth >= 0);
        assert!(maxdepth < 19);
        let dims = 3;
        let bx = relate::NdBox::new(vec![1066052, 1642769, 1899],
                                    vec![1083529, 1677722, 2057]);
    //    let bx = relate::NdBox::new(vec![0; dims], vec![512; dims]);
        let ranges = _nquery(bx, maxdepth, 63);
        println!("{:?}", ranges.len());
    }

}
