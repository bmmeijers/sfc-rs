use relate;
use hilbert::{initial_end, gray_decode_travel, child_start, child_end, pack_index};
use std::collections::VecDeque;
use std::env;

//def morton_zcode_ncode(dim, sort_by_ncode=False):
//    ary = nary(dim)
//    codes = [(zcode, bitreverse(zcode, dim)) for zcode in range(ary)]
//    if sort_by_ncode:
//        codes.sort(key=itemgetter(1))
//    return codes


// See: https://users.rust-lang.org/t/solved-how-to-export-vec-string-to-c-with-ffi/7121
// and:
// https://github.com/dns2utf8/hour_glass/blob/master/src/lib.rs

// Oh, I suppose giving C a Box<Vec<T>> would work too, and be a bit easier
// https://users.rust-lang.org/t/box-over-ffi-boundary/3303
//
// could a interface for C look like:
// https://stackoverflow.com/questions/37436779/is-it-possible-to-call-a-rust-function-taking-a-vec-from-c#37437838
// 
// bmmeijers: you could return a repr(C) struct that represents a boxed slice, basically
// where the ranges are just stored in the vec...
//extern crate libc;
//#[no_mangle]
//pub extern fn make_ranges() -> *mut Vec<i32> {
//    Box::into_raw(Box::new(Vec::new()))
//}
//#[no_mangle]
//pub extern fn drop_ranges(vec: *mut Vec<i32>)  {
//    unsafe {
//        assert!(!vec.is_null());
//        Box::from_raw(vec);
//    }
//}

// On the C-side:
// Add extern declarations
//int main(int argc, char *argv[]) {
//    void *v = make_vec(); // Use a real typedef here
//    add_number(v, 42);
//    print_vec(v);
//    drop_vec(v);
//}

fn nary(dim: usize) -> usize
{
    // how many childs does a nd-box have given the number of dimensions
    2usize.pow(dim as u32)
}

fn morton_zcode_ncode(dim: usize) -> Vec<usize> {
    let ary = nary(dim);
    let mut codes = Vec::with_capacity(dim);
    for zcode in 0..ary {
        codes.push(relate::bitreverse(zcode, dim));
    }
    codes
}

fn path_to_hilbert(path: &Vec<u64>, dim: usize, order: usize) -> [u64; 2]
{
//    """Given a traversed path in the tree (with integers that represent h-codes),
//    determine the hilbert code that starts in this box of the path
//    """
    let level = path.len();
//    # width: the size of the range belonging to this node at this level
    let width = 2u64.pow( (dim * (order-level)) as u32);
    let hcode = pack_index(path, dim) * width;
    [hcode, hcode + width]
}

//fn path_to_morton(path: &Vec<u64>, dim: usize, order: usize) -> (u64, u64)
//{
//    let mut diff = order - 1;
//    let mut start = 0;
//    for zorder in path {
//        let width = diff * dim;
//        start += zorder << width;
//        if diff > 1 {
//            diff -= 1;
//        }
//    }
//    let level = path.len();
//    let width = 2u64.pow( (dim * (order-level)) as u32);
//    (start, start + width)
//}

fn _hquery(query: relate::NdBox, maxdepth: usize, maxbits: usize) -> Vec<[u64; 2]> {
    assert!(maxdepth < 19);
    let dim = query.dims();
    let order = maxbits / dim;
    let hmask = 2usize.pow(dim as u32) - 1;
    let traverse_childs_order = morton_zcode_ncode(dim);
    // let maxdepth = 12;
    let l_root = vec![0; dim];
    let h_root = vec![2u64.pow(order as u32); dim];
    let root = relate::NdBox::new(l_root, h_root);
//    println!("{}", root);
    let mut deq = VecDeque::new(); // with_capacity(10_000_000); FIXME: estimate on how many we'll get? -- E.g. as percentage of total domain of largest range, squared?

    let hstart = 0;
    let hend = initial_end(order, dim);

    deq.push_back( (root, 1, (hstart, hend), vec![0u64;0]) );
    let mut ranges = Vec::new();
    while deq.len() > 0 {
//        println!("{}", deq.len());
        let value = match deq.pop_front() {
            Some(value) => value,
            None => panic!("Popping from empty VecDeque")
        };
        let cur_node = value.0;
        let cur_level = value.1;
        let cur_hstart = (value.2).0;
        let cur_hend = (value.2).1;
        let cur_path = value.3;
        let ndcmp = query.relate(&cur_node);
        match ndcmp {
            // -- disjoint, done: no need to process further
            relate::RelateResult::Disjoint => {},
            // -- equal or fully contains, done: add to result
            relate::RelateResult::Equals | relate::RelateResult::FullyContains => {
                let range = path_to_hilbert(&cur_path, dim, order as usize);
                ranges.push(range);
            },
            // -- partial overlap
            relate::RelateResult::Partial => {
                if cur_level < maxdepth {
                    let mut childs = Vec::new();
                    let next_level = cur_level + 1;
                    for n in &traverse_childs_order // 0.. nary(dim)
                    {
                        let ncode = *n;

                        let next_box = cur_node.nth_child_norder(ncode);
                        let hcode = gray_decode_travel(cur_hstart, cur_hend, hmask, ncode);

                        let mut new_path = cur_path.clone();
                        new_path.push(hcode as u64); // << hcode!

                        let next_start = child_start(cur_hstart, cur_hend, hmask, hcode);
                        let next_end = child_end(cur_hstart, cur_hend, hmask, hcode);

                        childs.push(
                            (hcode, next_box, next_level, (next_start, next_end), new_path)
                        );

                    }
                    childs.sort_by_key(|k| k.0);
                    for item in childs {
                        deq.push_back((item.1, item.2, item.3, item.4));
                    }
                } else {
                    let range = path_to_hilbert(&cur_path, dim, order as usize);
                    ranges.push(range);
                };
            }
        };
    }

    ranges
}

pub fn hquery(low: Vec<u64>, high: Vec<u64>, maxdepth: usize) -> Vec<[u64; 2]> {
    let bx = relate::NdBox::new(low, high);
//                                vec![1066052, 1642769, 1899], 
//                                vec![1083529, 1677722, 2057]);
    //    let bx = relate::NdBox::new(vec![0; dims], vec![512; dims]);
    let ranges = _hquery(bx, maxdepth, 63);
    ranges
}

pub fn main() {
    println!("{:?}", morton_zcode_ncode(3));

    let args: Vec<_> = env::args().collect();
    if args.len() > 1 {
        let maxdepth: usize = args[1].parse().unwrap();
        assert!(maxdepth >= 0);
        assert!(maxdepth < 19);
//        let dims = 3;
        let bx = relate::NdBox::new(vec![1066052, 1642769, 1899], 
                                    vec![1083529, 1677722, 2057]);
    //    let bx = relate::NdBox::new(vec![0; dims], vec![512; dims]);
        let ranges = _hquery(bx, maxdepth, 63);
        println!("{:?}", ranges.len());
        println!("{:?}", ranges);
    }

}
