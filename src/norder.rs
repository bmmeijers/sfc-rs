use std::cmp;

// morton n-order code

pub fn decode(val: u64, ndims: usize) -> Vec<u64> {
    let p = 2u64.pow(ndims as u32);  // Chunks are like digits in base 2**nD.
    let mbits = cmp::max(1, ((val + 1) as f64).log(p as f64).ceil() as usize);
    let path = mortoncode_to_path(val, mbits, ndims);
    let result = transpose_bits(&path, ndims);
    result
}

fn mortoncode_to_path(code: u64, mbits: usize, ndims: usize) -> Vec<u64>
{
    let mut path = vec![0; mbits];
    let mask = (1 << ndims) - 1;
    for i in 0..mbits {
        path[mbits - i - 1] = (code >> (i*ndims)) & mask
    }
    return path;
}

fn transpose_bits(srcsin: &Vec<u64>, n_dests: usize) -> Vec<u64> {
    let mut srcs = srcsin.clone();
    let n_srcs = srcs.len();
    let mut dests = vec![0; n_dests];
    for j in (0..n_dests).rev()
    {
        let mut dest = 0;
        for k in 0..n_srcs {
            dest = dest * 2 + srcs[k] % 2;
            srcs[k] /= 2;       // Divide by two, i.e. shift right (>>) with 1
        }
        dests[j] = dest;
    }
    dests
}

fn path_to_code(path: &Vec<u64>, ndims: usize) -> u64 {
    if path.len() > 0 {
        let p = 2u64.pow(ndims as u32);
        let mut tally = path[0];
        if path.len() > 1 {
            for i in 1..path.len() {
                let nxt = path[i];
                tally = tally * p + nxt;
            }
        }
        tally
    } else {
        0
    }
}

pub fn encode(coords: &Vec<u64>) -> u64 {
    let ndims = coords.len();
    let biggest = match coords.iter().max() {
        Some(biggest) => biggest,
        None    => panic!("no maximum value found in coords")
    };
    let n_chunks = cmp::max(1, ((biggest + 1) as f64).log(2.0).ceil() as usize);
    let path = transpose_bits(&coords, n_chunks);
    let result = path_to_code(&path, ndims);
    result
}

