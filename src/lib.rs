use std::mem;
use std::slice;
//use std::f64;
//use std::ffi::{CStr, CString};

mod hilbert;
mod norder;

mod query_norder;
mod query_horder;
mod relate;

// Later also look at what is done at getsentry:
// https://github.com/getsentry/libsourcemap/blob/41ca324a1aae54ddf8df3bbf57e8d7d86a200559/src/cabi.rs#L61-L87


extern crate libc;
use self::libc::{c_void, size_t};


#[repr(C)]
pub struct Array {
    pub data: *const c_void,
    pub len: size_t,
}


//#[no_mangle]
//pub extern "C" fn decode_polyline_ffi(pl: *const c_char, precision: uint32_t) -> Array {
//    let s: String = unsafe { CStr::from_ptr(pl).to_string_lossy().into_owned() };
//    arr_from_string(s, precision)
//}

/// Free Array memory which Rust has allocated across the FFI boundary
///
/// # Safety
///
/// This function is unsafe because it accesses a raw pointer which could contain arbitrary data
#[no_mangle]
pub extern "C" fn array_free(arr: Array) {
    if arr.data.is_null() {
        return;
    }
//    println!("Free'ing array at rust side ...");
    let _: Vec<_> = arr.into();
}


#[no_mangle]
pub extern "C" fn hdecode(input: u64, dims: size_t) -> Array {
//    println!("received at rust side := {}", input);
//    let original = vec![1, 2, 3, 4];
//    println!("received at rust side (hdecode) := {:?}", input);
    let result = hilbert::decode(input, dims);
    result.into()
}

#[no_mangle]
pub extern "C" fn hencode(input: Array) -> u64 {
//    println!("received at rust side (hencode) := {:?}", input.data);
//    let original = vec![1, 2, 3, 4];
    let vec: Vec<_> = input.into();
//    println!("{:?}", vec);
    let result = hilbert::encode(&vec);
    result.into()
}



#[no_mangle]
pub extern "C" fn ndecode(input: u64, dims: size_t) -> Array {
//    println!("received at rust side := {}", input);
//    let original = vec![1, 2, 3, 4];
    let result = norder::decode(input, dims);
    result.into()
}

#[no_mangle]
pub extern "C" fn nencode(input: Array) -> u64 {
//    println!("received at rust side := {}", input);
//    let original = vec![1, 2, 3, 4];
    let vec: Vec<_> = input.into();
    let result = norder::encode(&vec);
    result.into()
}


#[no_mangle]
pub extern "C" fn hquery(low: Array, high: Array, maxdepth: usize) -> Array { //Vec<[u64; 2]> {
    let v0: Vec<_> = low.into();
    let v1: Vec<_> = high.into();
    query_horder::hquery(v0, v1, maxdepth).into()
}


#[no_mangle]
pub extern "C" fn nquery(low: Array, high: Array, maxdepth: usize) -> Array { //Vec<[u64; 2]> {
    let v0: Vec<_> = low.into();
    let v1: Vec<_> = high.into();
    query_norder::nquery(v0, v1, maxdepth).into()
}


// Build an Array from , so it can be leaked across the FFI boundary
// Note, T is the type of the values stored inside the Vec
impl<T> From<Vec<T>> for Array {
    fn from(sl: Vec<T>) -> Self {
        let array = Array {
            data: sl.as_ptr() as *const c_void,
            len: sl.len() as size_t,
        };
        mem::forget(sl); // leak
        array
    }
}

//impl From<Vec<[u64;2]>> for Array {
//    fn from(sl: Vec<[u64;2]>) -> Self {
//        let array = Array {
//            data: sl.as_ptr() as *const c_void,
//            len: sl.len() as size_t,
//        };
//        mem::forget(sl); // leak
//        array
//    }
//}


// Build Vec from an Array, so it can be dropped
impl From<Array> for Vec<u64> {
    fn from(arr: Array) -> Self {
//        println!("instantiating vec from array");
        unsafe { slice::from_raw_parts(arr.data as *mut u64, arr.len).to_vec() }
    }
}



#[cfg(test)]
mod tests {
    use super::*;
//    use std::ptr;
//    use std::ffi::{CString, CStr};

    #[test]
    fn test_array_conversion() {
        let original = vec![1, 2, 3, 4];
        // move into an Array, and leak it
        let arr: Array = original.into();
        // move back into a Vec -- leaked value still needs to be dropped
        let converted: Vec<_> = arr.into();
        assert_eq!(&converted, &[1, 2, 3, 4]);
        // drop it
        array_free(converted.into());
    }

    #[test]
    fn test_produce_array() {
        let arr = produce_array(5);
        array_free(arr);
    }
}
