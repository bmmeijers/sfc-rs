use std::fmt;

pub fn bitreverse(mut n: usize, bits: usize) -> usize
{
    let m = 1 << bits;           // # find N: shift left 1 by the number of bits
    let mut nrev = n;                // # nrev will store the bit-reversed pattern
    for _ in 1..bits {
        n >>= 1;
        nrev <<= 1;
        nrev |= n & 1;       // # give LSB of n to nrev
    }
    nrev &= m - 1;           // # clear all bits more significant than N-1
    nrev
}


#[derive(Debug)]
pub enum RelateResult {
    Disjoint,
    Equals,
    FullyContains,
    Partial
}

#[derive(Debug)]
pub struct NdBox {
    lo: Vec<u64>,
    hi: Vec<u64>
}

impl NdBox {
    pub fn new(lo: Vec<u64>, hi: Vec<u64>) -> NdBox {
        assert_eq!(lo.len(), hi.len());
        NdBox {
            lo: lo,
            hi: hi
        }
    }

    pub fn dims(&self) -> usize {
        self.lo.len()
    }

    pub fn nth_child_norder(&self, n: usize) -> NdBox {
        let mut c = vec![0; self.dims()];
        let out = self.lo.iter().zip(self.hi.iter());
        for (d, (x, y)) in out.enumerate() {
            c[d] = x + (y-x) / 2;
        }
        let mut l = self.lo.clone();
        let mut h = self.hi.clone();
        let dim = self.dims();
        for d in 0..dim {
            let rshifted = bitreverse(n, dim) >> d;
            let at_high_side_for_dim_d = (rshifted & 1) == 1;
            if at_high_side_for_dim_d {
                l[d] = c[d]
            } else {
                // keep the low, shift the high ordinate to the center
                h[d] = c[d]
            }
        }
        NdBox::new(l, h)
    }

    pub fn relate(&self, other: &NdBox) -> RelateResult {
        // Make enum for result type ??
        // how many dimensions to check?
        let dims = self.dims();
        let mut ncmp = 1;
        // equal, all coordinates are equal
        for d in 0 .. dims  {
            ncmp &= (self.lo[d] == other.lo[d] && self.hi[d] == other.hi[d]) as usize;
        }
        if ncmp == 1 {
            return RelateResult::Equals; // 0
        }

        // fully contains, rect fully contains qrt
        ncmp = 1;
        for d in 0 .. dims {
            ncmp &= (self.lo[d] <= other.lo[d] && self.hi[d] >= other.hi[d]) as usize;
        }
        if ncmp == 1 {
            return RelateResult::FullyContains; // 1
        }

        // intersects, the two nd-boxes interact 
        // (either on the boundary or internally)
        ncmp = 1;
        for d in 0 .. dims {
            ncmp &= (self.lo[d] < other.hi[d] && self.hi[d] > other.lo[d]) as usize;
        }
        if ncmp == 1{
            return RelateResult::Partial; // 2
        }
        //# no overlap
        return RelateResult::Disjoint; // -1
    }

}

impl fmt::Display for NdBox {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "NdBox(vec!{:?}, vec!{:?})", self.lo, self.hi)
    }
}

//fn bitreverse_alt(n: usize, bits: usize) -> usize
//{
//    //assert_eq!(bits % 2, 0);
//    let mut var = 0;
//    for i in 0 .. bits / 2 { // FIXME: if bits uneven this does not work
//        let p = bits - i - 1;
//        let mut x = n & (1 << p);
//        x >>=  p;
//        let mut y = n & (1 << i);
//        y >>=  i;
//        var |= (x << i);
//        var |= (y << p);
//    }
//    var
//}

pub fn main() {

    for n in 0..32 {
        bitreverse(n, 6);
    }

    let bx = NdBox::new(vec![0; 3], vec![512; 3]);
//    println!("{} {}", bx, bx.dims());
    for n in 0..8 {
        println!("{}", bx.nth_child_norder(n));
    }

    let one = NdBox::new(vec![0, 0, 0], vec![10, 10, 10]);
    let other = NdBox::new(vec![0, 0, 0], vec![10, 10, 10]);
    println!("{:?}", one.relate(&other));

    let one = NdBox::new(vec![0, 0, 0], vec![10, 10, 10]);
    let other = NdBox::new(vec![20, 20, 20], vec![30, 30, 30]);
    println!("{:?}", one.relate(&other));
}
