use std::cmp;

//#![feature(test)]
//extern crate test;

// hilbert code

//from libc.math cimport log, ceil
//#import array
//#from cpython cimport array
//import cython


//def decode(object i, int nD=2):  # Default is the 2D Hilbert walk.
//    """Decode a Hilbert key as nD coordinate"""
//    cdef int j, nChunks, mask, start, end, parent_start, parent_end
//    index_chunks = unpack_index(i, nD)
//    nChunks = len(index_chunks)
//    mask = 2 ** nD - 1
//    start = 0
//    end = initial_end(nChunks, nD)
//    coord_chunks = [0] * nChunks
//    for j in range(nChunks):
//        i = index_chunks[j]
//        coord_chunks[j] = gray_encode_travel(start, end, mask, i)
//        parent_start = start
//        parent_end = end
//        start = child_start(parent_start, parent_end, mask, i)
//        end = child_end(parent_start, parent_end, mask, i)
//#    print "d: index chunks", index_chunks
//#    print "d: coord chunks", coord_chunks
//#    return tuple(pack_coords(coord_chunks, nD)), coord_chunks, index_chunks, val
//    return tuple(pack_coords(coord_chunks, nD))


pub fn decode(val: u64, ndims: usize) -> Vec<u64> {
    let mask = 2usize.pow(ndims as u32) - 1;  //2**nD -1
    let index_chunks = unpack_index(val, ndims);
    let n_chunks = index_chunks.len();

    let mut start = 0;
    let mut end = initial_end(n_chunks, ndims);
    let mut coord_chunks = vec![0; n_chunks];
    for j in 0..n_chunks {
        let i = index_chunks[j];
        coord_chunks[j] = gray_encode_travel(start, end, mask, i as usize) as u64;
        let parent_start = start;
        let parent_end = end;
        start = child_start(parent_start, parent_end, mask, i as usize);
        end = child_end(parent_start, parent_end, mask, i as usize);
    }
    pack_coords(&coord_chunks, ndims)
}


//cdef inline unpack_index(object i, int nD):
//    cdef int p, j, nChunks
//    p = 2**nD     # Chunks are like digits in base 2**nD.
//    nChunks = max(1, int(ceil(log(i + 1)/log(p))))  # of digits
//    chunks = [0] * nChunks
//    for j in range(nChunks - 1, -1, -1):
//        chunks[j] = i % p
//        i = i / p
//    return chunks


fn unpack_index(i: u64, ndims: usize) -> Vec<u64>
{
    let mut ii = i as usize;
    let p = 2usize.pow(ndims as u32);
    let n_chunks = cmp::max(1, ((i + 1) as f64).log(p as f64).ceil() as usize);
    let mut chunks = vec![0; n_chunks];
    for j in (0..n_chunks).rev()
    {
        chunks[j] = (ii % p) as u64;
        ii = ii / p;
    }
    chunks
}


//def encode(coords):
//    """Encode a nD coordinate as Hilbert key"""
//    cdef int i, j, nChunks, mask, start, end, parent_start, parent_end
//    nD = len(coords)
//    coord_chunks = unpack_coords(coords)
//    nChunks = len(coord_chunks)
//    mask = 2 ** nD - 1 # a number with all bits set to 1 and has as many bits as there are dimensions, so for 2D the mask is 11, for 3D: 111, for 4D: 1111, etc.
//    start = 0 
//    end = initial_end(nChunks, nD)
//    index_chunks = [0] * nChunks
//    for j in range(nChunks):
//        i = gray_decode_travel(start, end, mask, coord_chunks[j]) # << coord_chunks[j] :=> NORDER VALUE
//        index_chunks[j] = i
//        parent_start = start
//        parent_end = end
//        start = child_start(parent_start, parent_end, mask, i)
//        end = child_end(parent_start, parent_end, mask, i)
//#        start, end = child_start_end(start, end, mask, i) # << i :=> CURRENT HINDEX 
//#    print ""
//#    print "e: coord chunks", coord_chunks # << in the n-ary tree this is the local N-ORDER identifier
//#    print "e: index chunks", index_chunks # << in the n-ary tree this is the local Hilbert identifier 
//    # (Note, it is *not* possible to us this index value to know which field you are in (as is the case with morton) how the curve was oriented previously has an influence on this...)!
//#    return coords, coord_chunks, index_chunks, pack_index(index_chunks, nD)
//    return pack_index(index_chunks, nD)


pub fn encode(coords: &Vec<u64>) -> u64 {
    let n_d = coords.len();
    let coord_chunks = unpack_coords(coords);
    let n_chunks = coord_chunks.len();
    let mask = 2usize.pow(n_d as u32) - 1;

    let mut start = 0;
    let mut end = initial_end(n_chunks, n_d);

    let mut index_chunks = vec![0; n_chunks];
//    println!("{:?}", coord_chunks);
//    println!("{}", n_chunks);    
//    println!("{} {}", start, end);
//    println!("chunks_b: {:?} {}", index_chunks, index_chunks.len());
    for j in 0..n_chunks {
        let i = gray_decode_travel(start, end, mask, coord_chunks[j] as usize);
        index_chunks[j] = i as u64;
//        println!("i:={}", i);
        let parent_start = start;
        let parent_end = end;
        start = child_start(parent_start, parent_end, mask, i);
        end =   child_end(  parent_start, parent_end, mask, i);
    }
//    println!("chunks_a:  {:?}", index_chunks);
    let result = pack_index(&index_chunks, n_d);
    result
}



//cdef int initial_end(int nChunks, int nD):
//    # This orients the largest cube so that
//    # its start is the origin (0 corner), and
//    # the first step is along the x axis, regardless of nD and nChunks:
//    return 2**((-nChunks - 1) % nD)  # in Python 0 <=   a % b   < b.

#[inline]
fn modulo(x: i32, n: i32) -> i32 {
    (x % n + n) % n
}

pub fn initial_end(n_chunks: usize, n_d: usize) -> usize {
    let s = -(n_chunks as i32) - 1;
    let t = modulo(s, n_d as i32);
    2usize.pow(t as u32)
}

//# Unpacking arguments and packing results of int <-> Hilbert functions.
//# nD == # of dimensions.
//# A "chunk" is an nD-bit int (or Python long, aka bignum).
//# Lists of chunks are highest-order first.
//# Bits within "coord chunks" are x highest-order, y next, etc.,
//# i.e., the same order as coordinates input to Hilbert_to_int()
//# and output from int_to_Hilbert().


//# unpack_index( int index, nD ) --> list of index chunks.
//cdef inline unpack_index(object i, int nD):
//    cdef int p, j, nChunks
//    p = 2**nD     # Chunks are like digits in base 2**nD.
//    nChunks = max(1, int(ceil(log(i + 1)/log(p))))  # of digits
//    chunks = [0] * nChunks
//    for j in range(nChunks - 1, -1, -1):
//        chunks[j] = i % p
//        i = i / p
//    return chunks


//cdef inline pack_index(chunks, nD):
//    p = 2**nD  # Turn digits mod 2**nD back into a single number:
//    return reduce(lambda n, chunk: n * p + chunk, chunks)


pub fn pack_index(path: &Vec<u64>, ndims: usize) -> u64 {
    if path.len() > 0 {
        let p = 2u64.pow(ndims as u32);
        let mut tally = path[0];
        if path.len() > 1 {
            for i in 1..path.len() {
                let nxt = path[i];
                tally = tally * p + nxt;
            }
        }
        tally
    } else {
        0
    }
}

//# unpack_coords( list of nD coords ) --> list of coord chunks each nD bits.
//cdef inline unpack_coords(coords):
//    cdef int nChunks, nD
//    nD = len(coords)
//    # biggest = reduce( max, coords )  # the max of all coords
//    biggest = max(coords)
//    nChunks = max(1, int(ceil(log(biggest + 1)/ log(2))))  # max # of bits
//    return transpose_bits(coords, nChunks)


fn unpack_coords(coords: &Vec<u64>) -> Vec<u64>
{
//    let ndims = coords.len();
    let biggest = match coords.iter().max() {
        Some(biggest) => biggest,
        None    => panic!("no maximum value found in coords")
    };
    let n_chunks = cmp::max(1, ((biggest + 1) as f64).log(2.0).ceil() as usize);
    let path = transpose_bits(&coords, n_chunks);
    path
}

//cdef inline pack_coords(chunks, nD):
//    return transpose_bits(chunks, nD)


//# transpose_bits --
//#    Given nSrcs source ints each nDests bits long,
//#    return nDests ints each nSrcs bits long.
//#    Like a matrix transpose where ints are rows and bits are columns.
//#    Earlier srcs become higher bits in dests;
//#    earlier dests come from higher bits of srcs.
//cdef inline transpose_bits(object srcarg, int nDests):
//    cdef int dest, k, j, nSrcs
//    srcs = list(srcarg) # Make a copy we can modify safely.
//    nSrcs = len(srcs)
//    dests = [0] * nDests
//    #dests = [0] * nDests
//    # Break srcs down least-significant bit first, shifting down:
//    for j in range(nDests - 1, -1, -1):
//        # Put dests together most-significant first, shifting up:
//        dest = 0
//        for k in range(nSrcs):
//            dest = dest * 2 + srcs[k] % 2
//            srcs[k] = srcs[k] / 2
//        dests[j] = dest
//    return dests


fn pack_coords(chunks: &Vec<u64>, n_dests: usize) -> Vec<u64> {
    transpose_bits(chunks, n_dests)
}

//cdef inline pack_coords(chunks, nD):
//    return transpose_bits(chunks, nD)

#[inline]
fn transpose_bits(srcsin: &Vec<u64>, n_dests: usize) -> Vec<u64> {
    let mut srcs = srcsin.clone();
    let n_srcs = srcs.len();
    let mut dests = vec![0; n_dests];
    for j in (0..n_dests).rev()
    {
        let mut dest = 0;
        for k in 0..n_srcs {
            dest = dest * 2 + srcs[k] % 2;
            srcs[k] /= 2;       // Divide by two, i.e. shift right (>>) with 1
        }
        dests[j] = dest;
    }
    dests
}


//cdef inline int gray_encode(int bn):
//    # Gray encoder and decoder from http://en.wikipedia.org/wiki/Gray_code
//#    assert bn >= 0
//#    assert type(bn) in [int, long]
//    return bn ^ (bn / 2)

#[inline]
fn gray_encode(bn: usize) -> usize {
    bn ^ (bn / 2)
}

//cdef inline int gray_decode(int n):
//    sh = 1
//    while True:
//        div = n >> sh
//        n ^= div
//        if div <= 1:
//            return n
//        sh <<= 1


#[inline]
fn gray_decode(n: usize) -> usize {
    let mut out = n;
    let mut sh = 1;
    loop {
        let div = out >> sh;
        out ^= div;
        if div <= 1 {
            return out;
        }
        sh <<= 1;
    }
}


//# gray_encode_travel -- gray_encode given start and end using bit rotation.
//#    Modified Gray code.  mask is 2**nbits - 1, the highest i value, so
//#        gray_encode_travel( start, end, mask, 0 )    == start
//#        gray_encode_travel( start, end, mask, mask ) == end
//#        with a Gray-code-like walk in between.
//#    This method takes the canonical Gray code, rotates the output word bits,
//#    then xors ("^" in Python) with the start value.

//@cython.cdivision(True)
//cdef inline int gray_encode_travel(int start, int  end, int mask, int i):
//    cdef int travel_bit = start ^ end
//    cdef int modulus = mask + 1          # == 2**nBits
//    # travel_bit = 2**p, the bit we want to travel.
//    # Canonical Gray code travels the top bit, 2**(nBits-1).
//    # So we need to rotate by ( p - (nBits-1) ) == (p + 1) mod nBits.
//    # We rotate by multiplying and dividing by powers of two:
//    cdef int g = gray_encode(i) * (travel_bit * 2)
//    return ((g | (g / modulus)) & mask) ^ start



//    cdef int travel_bit = start ^ end
//    cdef int modulus = mask + 1          # == 2**nBits
//    # travel_bit = 2**p, the bit we want to travel.
//    # Canonical Gray code travels the top bit, 2**(nBits-1).
//    # So we need to rotate by ( p - (nBits-1) ) == (p + 1) mod nBits.
//    # We rotate by multiplying and dividing by powers of two:
//    cdef int g = gray_encode(i) * (travel_bit * 2)
//    return ((g | (g / modulus)) & mask) ^ start


#[inline]
fn gray_encode_travel(start: usize, end: usize, mask: usize, i: usize) -> usize
{
    let travel_bit = start ^ end;
    let modulus = mask + 1;
    let g = gray_encode(i) * (travel_bit * 2);
    let result = ((g | (g / modulus)) & mask) ^ start;
    result
}

//@cython.cdivision(True)
//cdef inline int gray_decode_travel(int start, int end, int mask, int g):
//    cdef int travel_bit = start ^ end
//    cdef int modulus = mask + 1          # == 2**nBits
//    cdef int rg = (g ^ start) * (modulus / (travel_bit * 2))
//#    print 'rg', rg
//    cdef int result = gray_decode((rg | (rg / modulus)) & mask)
//#    print 'rgresult', result
//    return result


#[inline]
pub fn gray_decode_travel(start: usize, end: usize, mask: usize, g: usize) -> usize
{
    let travel_bit = start ^ end;
    let modulus = mask + 1;          // # == 2**nBits
    let rg = (g ^ start) * (modulus / (travel_bit * 2));
    let result = gray_decode((rg | (rg / modulus)) & mask);
    result
}


//# child_start_end( parent_start, parent_end, mask, i ) -- Get start & end for child.
//#    i is the parent's step number, between 0 and mask.
//#    Say that parent( i ) =
//#           gray_encode_travel( parent_start, parent_end, mask, i ).
//#    And child_start(i) and child_end(i) are what child_start_end()
//#    should return -- the corners the child should travel between
//#    while the parent is in this quadrant or child cube.
//#      o  child_start( 0 ) == parent( 0 )       (start in a corner)
//#      o  child_end( mask ) == parent( mask )   (end in a corner)
//#      o  child_end(i) - child_start(i+1) == parent(i+1) - parent(i)
//#         (when parent bit flips, same bit of child flips the opposite way)
//# Those constraints still leave choices when nD (# of bits in mask) > 2.
//#    Here is how we resolve them when nD == 3 (mask == 111 binary),
//#    for parent_start = 000 and parent_end = 100 (canonical Gray code):
//#         i   parent(i)    child_
//#         0     000        000   start(0)    = parent(0)
//#                          001   end(0)                   = parent(1)
//#                 ^ (flip)   v
//#         1     001        000   start(1)    = parent(0)
//#                          010   end(1)                   = parent(3)
//#                ^          v
//#         2     011        000   start(2)    = parent(0)
//#                          010   end(2)                   = parent(3)
//#                 v          ^
//#         3     010        011   start(3)    = parent(2)
//#                          111   end(3)                   = parent(5)
//#               ^          v
//#         4     110        011   start(4)    = parent(2)
//#                          111   end(4)                   = parent(5)
//#                 ^          v
//#         5     111        110   start(5)    = parent(4)
//#                          100   end(5)                   = parent(7)
//#                v          ^
//#         6     101        110   start(6)    = parent(4)
//#                          100   end(6)                   = parent(7)
//#                 v          ^
//#         7     100        101   start(7)    = parent(6)
//#                          100   end(7)                   = parent(7)
//#    This pattern relies on the fact that gray_encode_travel()
//#    always flips the same bit on the first, third, fifth, ... and last flip.
//#    The pattern works for any nD >= 1.
//#
//cdef int  child_start(int parent_start, int parent_end, int mask, int i):
//    cdef int start_i = max(0, (i - 1) & ~1)  # next lower even number, or 0
//    cdef int child_start = gray_encode_travel(parent_start, parent_end, mask, start_i)
//    return child_start


#[inline]
pub fn child_start(parent_start: usize, parent_end: usize, mask: usize, i: usize) -> usize
{
    let start_i = cmp::max(0, ((i as i64) - 1) & (!1)) as usize; // # next lower even number, or 0
    let child_start = gray_encode_travel(parent_start, parent_end, mask, start_i);
    child_start as usize
}

//cdef int child_end(int parent_start, int parent_end, int mask, int i):
//    cdef int end_i = min(mask, (i + 1) | 1)  # next higher odd number, or mask
//    cdef int child_end = gray_encode_travel(parent_start, parent_end, mask, end_i)
//    return child_end


#[inline]
pub fn child_end(parent_start: usize, parent_end: usize, mask: usize, i: usize) -> usize
{
    let end_i = cmp::min(mask, (i + 1) | 1); // # next lower even number, or 0
    let child_end = gray_encode_travel(parent_start, parent_end, mask, end_i);
    child_end
}


#[cfg(test)]
mod tests {
    use super::*;

    fn encdec(maxsize: u64, 
            encode: fn(&Vec<u64>) -> u64, 
            decode: fn(u64, usize) -> Vec<u64>) -> ()
    {
        for x in 0..maxsize {
            for y in 0..maxsize {
                let c = vec![x, y];
                let e = encode(&c);
                let d = decode(e, 2);
                assert_eq!(c, d);
            }
        }

        for x in 0..maxsize {
            for y in 0..maxsize {
                for z in 0..maxsize {
                    let c = vec![x, y, z];
                    let e = encode(&c);
                    let d = decode(e, 3);
                    assert_eq!(c, d);
                }
            }
        }

        for x in 0..maxsize {
            for y in 0..maxsize {
                for z in 0..maxsize {
                    for t in 0..maxsize {
                        let c = vec![x, y, z, t];
                        let e = encode(&c);
                        let d = decode(e, 4);
                        assert_eq!(c, d);
                    }
                }
            }
        }

        for x in 0..maxsize {
            for y in 0..maxsize {
                for z in 0..maxsize {
                    for t in 0..maxsize {
                        for u in 0..maxsize {
                            let c = vec![x, y, z, t, u];
                            let e = encode(&c);
                            let d = decode(e, 5);
                            assert_eq!(c, d);
                        }
                    }
                }
            }
        }
    }

    #[test]
    fn test_enc() {
        let exp = 4u32;
        let maxsize = 2u64.pow(exp);
        for x in 0..maxsize {
            for y in 0..maxsize {
                let c = vec![x, y];
                let e = encode(&c);
            }
        }
    }

    #[test]
    fn test_something() {
        let exp = 4u32;
        let maxsize = 2u64.pow(exp);
        encdec(maxsize, encode, decode);
    }
}

