//mod hilbert;
//mod norder;

//fn test_encdec(maxsize: u64, 
//        encode: fn(&Vec<u64>) -> u64, 
//        decode: fn(u64, usize) -> Vec<u64>) -> ()
//{
//    for x in 0..maxsize {
//        for y in 0..maxsize {
//            let c = vec![x, y];
//            let e = encode(&c);
//            let d = decode(e, 2);
//            assert_eq!(c, d);
//        }
//    }

//    for x in 0..maxsize {
//        for y in 0..maxsize {
//            for z in 0..maxsize {
//                let c = vec![x, y, z];
//                let e = encode(&c);
//                let d = decode(e, 3);
//                assert_eq!(c, d);
//            }
//        }
//    }

//    for x in 0..maxsize {
//        for y in 0..maxsize {
//            for z in 0..maxsize {
//                for t in 0..maxsize {
//                    let c = vec![x, y, z, t];
//                    let e = encode(&c);
//                    let d = decode(e, 4);
//                    assert_eq!(c, d);
//                }
//            }
//        }
//    }

//    for x in 0..maxsize {
//        for y in 0..maxsize {
//            for z in 0..maxsize {
//                for t in 0..maxsize {
//                    for u in 0..maxsize {
//                        let c = vec![x, y, z, t, u];
//                        let e = encode(&c);
//                        let d = decode(e, 5);
//                        assert_eq!(c, d);
//                    }
//                }
//            }
//        }
//    }
//}


//fn main() {
//    let exp = 5u32;
//    let maxsize = 2u64.pow(exp);
//    let mut s = 0;
//    for i in 0..6 {
//        s += 2u64.pow(exp * i);
//    }
//    println!("Enc/dec: {}", s);

//    use std::time::Instant;
//    let now = Instant::now();

//    {
//        test_encdec(maxsize, hilbert::encode, hilbert::decode);
//    }

//    let elapsed = now.elapsed();
//    let sech = (elapsed.as_secs() as f64) + (elapsed.subsec_nanos() as f64 / 1000_000_000.0);
//    println!("Seconds h: {}", sech);

//    // -------------------------------------------------

//    let now = Instant::now();
//    {
//        test_encdec(maxsize, norder::encode, norder::decode);
//    }
//    let elapsed = now.elapsed();
//    let secm = (elapsed.as_secs() as f64) + (elapsed.subsec_nanos() as f64 / 1000_000_000.0);
//    println!("Seconds m: {}", secm);

//    println!("Ratio h / m: {}", sech / secm);

//    println!("Tested encoding - decoding");
//}

// -----
//mod relate;

//fn main() {
//    relate::main();
//}

// ---
//mod query_norder;
//mod relate;

//fn main() {
//    query_norder::main();
//}


mod query_horder;
mod relate;
mod hilbert;

fn main() {
    query_horder::main();
}


//fn main() {
//    for i in 0..16777216 {
//        let result = hilbert::decode(i, 3);
//    }
//    println!("decoded 16777216");
//}
